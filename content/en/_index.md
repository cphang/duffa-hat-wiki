---
title: DUFFA Hat Wiki
---

{{< blocks/cover title="DUFFA Hat Wiki" image_anchor="top" height="full" >}}
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/">
  Learn More <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://gitlab.com/cphang/duffa-hat-wiki">
  Download <i class="fab fa-gitlab ms-2 "></i>
</a>
<p class="lead mt-5">The one stop shop for running DUFFA Hat!</p>
{{< blocks/link-down color="info" >}}
{{< /blocks/cover >}}


{{% blocks/section color="dark" type="row" %}}

{{% blocks/feature icon="fab fa-gitlab" title="Contributions welcome!" url="https://gitlab.com/cphang/duffa-hat-wiki/-/merge_requests" %}}
Edit the wiki with a [Merge Request](https://gitlab.com/cphang/duffa-hat-wiki/-/merge_requests). New users are always welcome!
{{% /blocks/feature %}}


{{% blocks/feature icon="fab fa-twitter" title="DUFFA Hat Twitter" url="https://twitter.com/duffafrisbee?lang=en-GB" %}}
Twitter announcements about the Hat
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-facebook" title="DUFFA Hat Facebook Group" url="https://www.facebook.com/groups/20991885689/?locale=en_GB" %}}
Facebook announcements about the Hat
{{% /blocks/feature %}}


{{% /blocks/section %}}
