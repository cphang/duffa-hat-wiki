---
title: Getting to Signups
description: Finding a venue, Working out a theme, setting a signup price. Everything you need to be able to announce that a DUFFA Hat is happening!
weight: 1
---

You want to run a DUFFA Hat? Fantastic news! There's a few things you need to decide before you announce that it's happening, and that folks can signup.

1. A theme
2. A venue
3. A date
4. A price
5. Number of participants

## A theme

Should be interesting enough to be able to:
- Come up with enough team names (at least 24!)
- Come up with enough pitch names (at least 6!)
- Crafting ideas for props and venue decorations
- Crafting ideas for all the prizes
- Designs for discs, shirts etc.

Coming up with good ideas here is often one of the most fun parts of planning a hat. To involve as many folks as possible,
build excitement and share ideas a 'themagining' event where the above is worked out together works very well.

Here is the [2019 one](https://docs.google.com/spreadsheets/d/1W73z33U9ridwYdMnJTgLWAeWILQnH_YkOfMgCqiF81k/edit#gid=0) where
the 'back from the dead' theme came to life, and some of the other themes that didn't quite make the final cut were discussed!

Looking back at [past events](https://www.duffa.org/duffa-hat-tournament/duffa-hat-history) is also a great way to get some
inspiration.

## A venue

## A date

## A price

## Number of participants

