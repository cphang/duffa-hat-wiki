---
title: About DUFFA Hat
linkTitle: About
menu: {main: {weight: 10}}
---

{{% blocks/cover title="About DUFFA Hat" image_anchor="bottom" height="auto" %}}

What is DUFFA Hat?
{.mt-5}

{{% /blocks/cover %}}

{{% blocks/lead %}}

What is DUFFA Hat? Our tournament has a reputation for being very fun, very spirited, and very well organised.:

It is two days of awesome ultimate – at least six matches and:
  - DUFFA Hat athletic shirt unique to your team
  - Friday night karaoke welcome party
  - Free meal on Saturday evening
  - Saturday night fancy dress party with FREE 3-pint challenges
  - Sideline games and team challenges
  - Free camping
  - More prizes than you can shake a stick at.
  - A festival atmosphere and loads of new friends for everyone
  - Our very special twelfth hat will have some surprises!

{{% /blocks/lead %}}
